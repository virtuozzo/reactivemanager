name := "ReactiveManager"

version := "0.1"

scalaVersion := "2.11.6"


libraryDependencies ++= Seq(
  "com.typesafe.akka"       %%    "akka-actor"                              %   "2.3.11",
  "com.typesafe.akka"       %%    "akka-remote"                             %   "2.3.11",
  "com.typesafe.akka"       %     "akka-stream-experimental_2.11"           %   "1.0-M5",
  "io.scalac"               %%    "reactive-rabbit"                         %   "0.2.2",
  "com.typesafe"            %     "config"                                  %   "1.2.1",
  "org.apache.logging.log4j" %    "log4j-core"                              %   "2.3",
  "org.apache.logging.log4j" %    "log4j-api"                               %   "2.3"

)
