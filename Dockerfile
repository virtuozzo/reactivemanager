FROM java:7
MAINTAINER Dmitry Kostyaev <dmitry@kostyaev.me>
ADD target/scala-2.11/ReactiveManager-assembly-0.1.jar /opt/ReactiveManager/
WORKDIR /opt/ReactiveManager
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "ReactiveManager-assembly-0.1.jar"]