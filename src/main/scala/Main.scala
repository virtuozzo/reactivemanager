import actors.Orchestrator
import akka.actor.{ActorSystem, Props}

import scala.language.postfixOps


object Main extends App {

  implicit val system = ActorSystem("ReactiveSystem")
  val actor = system.actorOf(Props[Orchestrator], "orchestrator")

}
