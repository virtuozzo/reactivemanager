package actors

import actors.messages.{Stop, Connect, CheckQueue, Register}
import akka.actor.{Actor, ActorRef}
import com.rabbitmq.client.ConnectionFactory
import com.typesafe.config.ConfigFactory
import org.apache.logging.log4j.LogManager
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

class Orchestrator extends Actor {

  val logger = LogManager.getLogger("Orchestrator")
  val conf = ConfigFactory.load()
  val addresses = conf.getConfigList("amqp.addresses")
  val host = addresses.get(0).getString("host")
  val port = addresses.get(0).getInt("port")
  val username = conf.getString("amqp.username")
  val password = conf.getString("amqp.password")
  val queue = conf.getString("amqp.queue")


  lazy val connection = {
    val factory = new ConnectionFactory()
    factory.setHost(host)
    factory.setPort(port)
    factory.setUsername(username)
    factory.setPassword(password)
    factory.newConnection()
  }

  lazy val channel = connection.createChannel()

  var freeNodes = IndexedSeq.empty[ActorRef]
  var busyNodes = IndexedSeq.empty[ActorRef]
  var listOfCounts = IndexedSeq.empty[Int]

  def getQueueSize: Int = {
    val size = channel.queueDeclarePassive(queue).getMessageCount
    listOfCounts = listOfCounts :+ size
    if (listOfCounts.size > 10)
      listOfCounts = listOfCounts.tail
    logger.info("Queue size is: " + size)
    size
  }

  def consumersSize: Int = channel.queueDeclarePassive(queue).getConsumerCount

  def avg = (listOfCounts.sum + getQueueSize) / (if (listOfCounts.size == 0) 1 else listOfCounts.size)

  override def preStart() = {
    context.system.scheduler.schedule(
      3 seconds, 1 minutes, self, CheckQueue)

  }

  override def receive: Receive = {
    case Register =>
      freeNodes = freeNodes :+ sender()
      logger.info("Nodes size reached " + (freeNodes.size + busyNodes.size))

    case CheckQueue if avg > 5000 || consumersSize < 1 =>
      freeNodes.headOption.foreach(node => {
        node ! Connect(host, port, username, password, queue)
        busyNodes = busyNodes :+ freeNodes.head
        freeNodes = freeNodes.tail
        logger.info("Average queue size is over 1000, adding new node")

      })
      logger.info("Number of free nodes " + freeNodes.size + ", number of busy nodes " + busyNodes.size)

    case CheckQueue if avg <= 5000 && consumersSize > 1 =>
      busyNodes.headOption.foreach(node => {
        node ! Stop
        freeNodes = freeNodes :+ busyNodes.head
        busyNodes = busyNodes.tail
        logger.info("Average queue size is less than 2, releasing one node")
      })
      logger.info("Number of free nodes " + freeNodes.size + ", number of busy nodes " + busyNodes.size)

    case _ => 

  }
}
